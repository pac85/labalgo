#pragma once


template <class LabelType>
struct Node {
    LabelType label;
    Node *left, *right;
    Node(LabelType label) {
        this.label = label;
        left = right = nullptr;
    }
};

template <class LabelType>
class Tree {
protected:

    Node<LabelType> *root;

    virtual void deleteNode(LabelType) = 0;
    virtual Node<LabelType>* findNode(LabelType);
    virtual void addNode(LabelType label);
    void printInOrder();
};

template <class LabelType>
struct AVLNode {
    Node<LabelType> inner;
    int height;
};

template <class LabelType>
class AVLTree: public Tree<AVLNode<LabelType>> {
    void rotateLeft(AVLNode<LabelType>*);
    void rotateRight(AVLNode<LabelType>*);
};
