#include <cstddef>
#include <vector>

class Graph {
    struct AdjNode {
        size_t node;
        AdjNode* next;
    };

private:
    std::vector<AdjNode*> adjList;
    size_t NumeE = 0;

    inline static void addToList(AdjNode *&list, size_t el) {
        auto node = new AdjNode();
        node->node = el;

        AdjNode *& last_node = list;
        while(last_node->next) last_node = last_node->next;
        last_node = node;
    }

    inline static bool searchInList(AdjNode *list, size_t el) {
        while(list) {
            if(list->node == el) 
                return true;
            list = list->next;
        }

        return false;
    }

    inline static size_t listLength(AdjNode *list) {
        size_t count = 0;
        while(count++, list) list = list->next;
        return count - 1;
    }

public:
    Graph(size_t v);

    inline size_t num_e() {
        return adjList.size();
    }
    void insert_edge(size_t v, size_t u);
    bool adjacent(size_t v, size_t u);
    AdjNode* neightbours(size_t v);
    size_t degree(size_t v);
};
