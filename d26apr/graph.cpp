#include "graph.hpp"

Graph::Graph(size_t v) {
    adjList.resize(v, nullptr);
}


void Graph::insert_edge(size_t v, size_t u) {
    Graph::addToList(adjList[v], u);
    Graph::addToList(adjList[u], v);
}

bool Graph::adjacent(size_t v, size_t u) {
    Graph::searchInList(adjList[v], u);
}

Graph::AdjNode* Graph::neightbours(size_t v) {
    return adjList[v];
}

size_t Graph::degree(size_t v) {
    return Graph::listLength(neightbours(v));
} 
