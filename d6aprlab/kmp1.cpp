#include <string>
#include <iostream>

int c_to_i(char c) {
    return c - 'a';
}

int ** kmp_dfa(std::string &pattern) {
    int ** dfa = new int*[26];
    for(size_t i = 0;i < 26; i++) {
        dfa[i] = new int[pattern.size()];
    }

    dfa[c_to_i(pattern[0])][0] = 1;
    for(int x = 0, j = 1;j < pattern.size();j++) {
        for(int c = 0;c < 26;c++) {
            dfa[c][j] = dfa[c][x];
        }
        dfa[c_to_i(pattern[j])][j] = j + 1;
        x = dfa[c_to_i(pattern[j])][x];
    }

    return dfa;
}

int count_occurs(int ** dfa, std::string &text, int m) {
    int i = 0, j = 0;
    for(; i < text.size() && j < m; i++) {
        j = dfa[c_to_i(text[i])][j];
    }

    return m == j;
}

int tandem(const std::string &pattern, const std::string &text) {
    size_t m = pattern.length(),
           n = text.length();
    
    std::string repeated = text + text;
    int ** dfa = kmp_dfa(repeated);


    int cmaxl = 0, maxl = 0;
    for(int i = 0, j = 0; i < text.size(); i++) {
        int next = dfa[c_to_i(text[i])][j];
        if(next <= j) {
            maxl = std::max(maxl, cmaxl);
            cmaxl = 0;
        }
        j = next;
        if(j == m * 2) j = m;
        if(j == m) cmaxl++;
    }

    return std::max(maxl, cmaxl);
}

int main() {
    std::string pattern("ab"), text("abaababababaaaababab");
    std::cout << tandem(pattern, text) << std::endl;
}
