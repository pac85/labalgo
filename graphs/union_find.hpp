#include <cstdlib>
#include <iostream>
#include <vector>

class UnionFind {
    std::vector<size_t> v;

public:
    inline UnionFind(size_t l): v(l){
        size_t i = 0;
        for(auto &el: v) {
            el = i++;
        }
    }

    inline size_t find(size_t i) {
        size_t c = v[i];
        while(c != v[c]) {
            c = v[c];
        }

        return c;
    }

    inline bool connected(size_t a, size_t b) {
        return find(a) == find(b);
    }

    inline bool set_union(size_t a, size_t b) {
        size_t ra = find(a), rb = find(b);
        if(ra != rb)
            v[ra] = rb;
        return ra == rb;
    }
};
