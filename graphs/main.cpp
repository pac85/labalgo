#include "union_find.hpp"
#include <algorithm>
#include <iostream>

struct Edge {
    size_t a, b;
    float weigth;
};

template<typename A, typename... T>
A mmax(A first, T... e) {
    A maxsf = first;
    ((maxsf = std::max(maxsf, e)), ...);

    return maxsf;
}

class Graph {
    std::vector<Edge> edges;

    Graph(std::vector<Edge> &v): edges(v) {}
public:
    Graph() {}

    size_t max_index() {
        size_t res = 0;
        for(auto &edge: edges) {
            res = mmax(res, edge.a, edge.b);
        }

        return res;
    }

    void add_edge(size_t a, size_t b, float w) {
        edges.push_back(Edge{a, b, w});
    }

    Graph kruskal() {
        UnionFind connected_set(max_index() + 1);
        std::vector<Edge> res;
        std::sort(edges.begin(), edges.end(), [](auto &a, auto &b){return a.weigth < b.weigth;});

        for(auto &edge: edges) {
            if(!connected_set.set_union(edge.a, edge.b)) {
                res.push_back(edge);
            }
        }

        return Graph{res};
    }

    void print_edges() {
        for(auto edge: edges) {
            std::cout << edge.a << "\t" << edge.b << "\t" << edge.weigth << std::endl;
        }
    }
};

int main() {
    Graph g;

    g.add_edge(0, 1, 4.0);
    g.add_edge(1, 2, 24.0);
    g.add_edge(2, 3, 23.0);
    g.add_edge(3, 4, 8.0);
    g.add_edge(4, 5, 10.0);
    g.add_edge(5, 6, 11.0);
    g.add_edge(6, 7, 7.0);
    g.add_edge(7, 4, 21.0);
    g.add_edge(4, 0, 16.0);
    g.add_edge(5, 2, 18.0);
    g.add_edge(0, 3, 6.0);
    g.add_edge(5, 7, 14.0);
    g.add_edge(3, 5, 5.0);
    g.add_edge(2, 6, 9.0);

    g.print_edges();

    std::cout << "Min spanning tree" << std::endl;
    g.kruskal().print_edges();
}
