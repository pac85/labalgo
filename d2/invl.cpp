#include <iostream>

struct llnode {
    int v;
    llnode* next;
};

llnode * from_array(int *array, size_t l) {
    llnode *first = new llnode{array[0], nullptr}, *c = first;

    for(size_t i = 1; i < l; i++) {
        llnode* n = new llnode{array[i], nullptr};
        c->next = n;
        c = n;
    }

    return first;
}

void print_llist(llnode *llist) {
    while(llist) {
        std::cout << llist->v << " ";
        llist = llist->next;
    }

    std::cout << std::endl;
}

llnode * invert_llist(llnode *llist, llnode *prev=nullptr) {
    llnode *next = llist->next;
    llist->next = prev;
    if(next == nullptr) return llist;
    return invert_llist(next, llist);
}

int main() {
    int a[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    llnode *list = from_array(a, 10);
    print_llist(list);
    list = invert_llist(list);
    print_llist(list);
}
