#include <iostream>
#include <random>

void print_array(int* array, size_t l) {
    for(size_t i = 0; i < l; i++) {
        std::cout << array[i] << " ";
    }

    std::cout << std::endl;
}

int* random_array(size_t l) {
    int* array = new int[l];
    for(size_t i = 0; i < l; i++) {
        array[i] = i;
    }

    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(0, l - 1);
    for(size_t i = l; i > 0; i--) {
        size_t j = distribution(generator);
        std::swap(array[i-1], array[j]);
    }

    return array;
}

int main() {
    print_array(random_array(10), 10);
}
