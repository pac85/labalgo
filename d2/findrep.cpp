#include <iostream>
#include <algorithm>
#include <cmath>

void print_array(int *array, size_t l) {
    for(size_t i = 0; i < l; i++) {
        std::cout << array[i] << " ";        
    }
    std::cout << std::endl;
}

//void partition(int *array, )
size_t partition(int *array, size_t l) {
    int pivot = array[int(floor((l-1)/2))];
    int a = -1;
    int b = l;
    for(;;) {
        do {
            a++;
        } while(array[a] < pivot);

        do {
            b--;
        } while(array[b] > pivot);

        if(a >= b) return b;

        std::swap(array[a], array[b]);
    }
}

void quicksort(int *array, size_t l) {
    if(l <= 1) return;

    size_t pivot_p = partition(array, l);

    quicksort(array, pivot_p + 1);
    quicksort(array + pivot_p + 1, l - (pivot_p + 1));
}

int findrep(int* array, size_t l) {
    quicksort(array, l);

    for(size_t i = 1; i < l; i++) {
        if (array[i-1] == array[i]) return i;
    }

    return -1;
}

int main() {
    int a[] = {1, 3, 3, -6};

    std::cout << findrep(a, 4) << std::endl;
}
