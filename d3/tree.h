struct bintree {
    int v;
    bintree *left, *right;
};

void preord_visit(bintree *node) {
    if(!node) return;
    std::cout << node->v << " ";
    preord_visit(node->left);
    preord_visit(node->right);
}

void inord_visit(bintree *node) {
    if(!node) return;
    inord_visit(node->left);
    std::cout << node->v << " ";
    inord_visit(node->right);
}

