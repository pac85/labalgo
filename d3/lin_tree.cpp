#include <iostream>

struct bintree {
    int v;
    bintree *left, *right;
};

bool is_linear(bintree *tree) {
    //no children
    if(!tree) return true;
    if(tree->left != nullptr && tree->right != nullptr ) return false;
    return is_linear(tree->left) && is_linear(tree->right);
}

int height(bintree *tree, int h = 0) {
    if(!tree) return h;
    return std::max(height(tree->left, h + 1), height(tree->right, h + 1));
}

void invert_copy_inner(bintree *tree, bintree *&out) {
    if(!tree) return;
    out = new bintree{tree->v, nullptr, nullptr};

    invert_copy_inner(tree->right, out->left);
    invert_copy_inner(tree->left, out->right);
}

bintree * invert_copy(bintree *tree) {
    bintree *root = nullptr;
    invert_copy_inner(tree, root);

    return root;
}

void invert(bintree *tree) {
    if(!tree) return;
    std::swap(tree->left, tree->right);
    invert(tree->left);
    invert(tree->right);
}

bool tree_eq(bintree *a, bintree *b) {
    if((a == nullptr) != (b == nullptr)) return false;
    if(a == nullptr && b == nullptr) return true;
    if(a->v != b->v) return false;

    return tree_eq(a->left, b->left) && tree_eq(a->right, b->right);
}
