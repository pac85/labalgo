#include <iostream>

struct bintree {
    int v;
    bintree *left, *right;
};

//false if v is found before c, true if v is found after c
bool left_right(int *array, bool *already_visited, bool &reached, size_t l, int c, int v) {
    bool cfound = false;
    reached = true;
    for(size_t i = 0; i < l; i++) {
        if(array[i] == c) {
            reached = true;
            cfound = true;
        }
        if(already_visited[i]) reached = false;
        if(array[i] == v) return cfound;
    }
    std::cout << "tryied to search " << v << " " << c << std::endl;
    exit(-1);
}

bool fleft(int *array, bool *already_visited, size_t l, int c, int v) {
    size_t ci;
    for(ci = 0; ci < l;ci++) {
        if(array[ci] == c) break;
    }
    if(ci <= 0) return false;
    ci--;
    //ci is te position of c
    for(;ci >= 0; ci--) {
        if(already_visited[ci]) return false;
        if(array[ci] == v) return true;
    }

    return false;
}

bool fright(int *array, bool *already_visited, size_t l, int c, int v) {
    size_t ci;
    for(ci = 0; ci < l;ci++) {
        if(array[ci] == c) break;
    }
    ci++;
    if(ci >= l) return false;
    //ci is te position of c
    for(;ci < l; ci++) {
        if(already_visited[ci]) return false;
        if(array[ci] == v) return true;
    }

    return false;
}

size_t find(int *array, size_t l, int v) {
    size_t ci;
    for(ci = 0; ci < l;ci++) {
        if(array[ci] == v) break;
    }

    return ci;
}

void inner(int *pre, int *inord, bool *already_visited, size_t l, size_t &c, bintree *&node) {
    if(c >= l) return;
    node = new bintree{pre[c], nullptr, nullptr};

    already_visited[find(inord, l, node->v)] = true;

    if(c + 1>= l) return;
    if(fleft(inord, already_visited, l, node->v, pre[c + 1])) {
        c++;
        inner(pre, inord, already_visited, l, c, node->left);
    }

    if(c + 1>= l) return;
    if(fright(inord, already_visited, l, node->v, pre[c + 1])) {
        c++;
        inner(pre, inord, already_visited, l, c, node->right);
    }
}

bintree * construct_from_prein(int *pre, int *inord, size_t l) {
    bintree *root;
    size_t c = 0;
    bool *already_visited = new bool[l];
    for(size_t i = 0;i < l; i++) {
        already_visited[i] = false;
    }
    inner(pre, inord, already_visited, l, c, root);
    delete[](already_visited);

    return root;
}

void preord_visit(bintree *node) {
    if(!node) return;
    std::cout << node->v << " ";
    preord_visit(node->left);
    preord_visit(node->right);
}

void inord_visit(bintree *node) {
    if(!node) return;
    inord_visit(node->left);
    std::cout << node->v << " ";
    inord_visit(node->right);
}

int main() {
    int pre[] = {4, 6, 7, 1, 2, 3};
    int inord[] = {7, 6, 1, 4, 2, 3};

    bintree *tree = construct_from_prein(pre, inord, 6);
    preord_visit(tree);
    std::cout << std::endl;
    inord_visit(tree);
    std::cout << std::endl;
}
