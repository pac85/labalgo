#include <iostream>
#include "tree.h"

void archimede_inner(int *array, int a, int b, bintree *&node) {
    if(b<a) return;
    int m = array[a];
    size_t mp = a;
    for(size_t i = a; i <= b; i++) {
        if(array[i] < m) {
            m = array[i];
            mp = i;
        }
    }

    node = new bintree{m, nullptr, nullptr};

    if(b==a) return;

    archimede_inner(array, a, mp - 1, node->left);
    archimede_inner(array, mp + 1, b, node->right);
}

bintree *archimede(int *array, size_t l) {
    bintree *root = nullptr;
    archimede_inner(array, 0, l - 1, root);

    return root;
}

int main() {
    int a[] = {9, 3, 7, 1, 8, 12, 10, 20, 15, 18, 5};
    bintree *tree = archimede(a, 11);
    inord_visit(tree);
    std::cout << std::endl;
}
