#include <iostream>
#include <stack>

char opposite(char c) {
    switch(c) {
        case '{': return '}';
        case '[': return ']';
        case '(': return ')';
    }
    return ' ';
}

bool check_parenthesis(std::string s) {
    std::stack<char> stack;
    for(auto c: s) {
        switch(c) {
            case ' ':
                continue;
            case '}': 
            case ']': 
            case ')': { 
                auto sc = stack.top();
                stack.pop();
                if (opposite(sc) != c) return false;
                break;
            }
            case '{':
            case '[':
            case '(':
                stack.push(c);
                break;
        }
    }

    return true;
}

int main() {
    std::string s;
    std::cin >> s;
    std::cout << check_parenthesis(s) << std::endl;
}
