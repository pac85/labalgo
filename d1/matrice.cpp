#include <iostream>

const int N = 4;

void print_mat(int n[N][N]) {
    for(int i = 0; i < N; i++) {
        for(int j = 0; j < N; j++) {
            std::cout << n[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

void print_mat(int ** n) {
    for(int i = 0; i < N; i++) {
        for(int j = 0; j < N; j++) {
            std::cout << n[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

int** transpose(int m[N][N]) {
    int ** res = new int*[N];
    for(int i = 0; i < N; i++) {
        res[i] = new int[N];
    }
    
    for(int i = 0; i < N; i++) {
        for(int j = 0; j < N; j++) {
            res[j][i] = m[i][j];
        }
    }

    return res;
}

int main() {

    int example[N][N] = { {1, 2, 3, 4}, 
                        {5, 6, 7, 8}, 
                        {9, 10, 11, 12},
                        {13, 14, 15, 16}};
    print_mat(example);

    std::cout << "transposed" << std::endl;
    print_mat(transpose(example));
}
