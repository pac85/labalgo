#include <iostream>

int maxsub(double * v, size_t n) {
    double max_ending_here = 0.0, 
           max_so_far = 0.0;

    for(size_t i = 0; i < n; i++) {
        max_ending_here = std::max(0.0, max_ending_here + v[i]);
        max_so_far = std::max(max_so_far, max_ending_here);
    }

    return max_so_far;
}

void print_v(double *v, size_t n) {
    for(size_t i = 0; i < n; i++) {
        std::cout << v[i] << " ";
    }
    std::cout << std::endl;
}

int main() {
    std::cout << "numero valori" << std::endl;
    size_t l;
    std::cin >> l;
    double* v = new double[l];
    std::cout << "valori" << std::endl;
    for(size_t i = 0; i < l; i++) {
        std::cin >> v[i];
    }

    std::cout << "somma massima"  << maxsub(v, l) << std::endl;
}
