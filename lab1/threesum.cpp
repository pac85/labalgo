#include <iostream>
#include <algorithm>
#include <cmath>

bool binsearch(int *array, size_t a, size_t b, int v) {
    if(array[a] > v || array[b] < v) return false;
    while(a <= b) {
        int m = (a + b) / 2;
        if(array[m] == v) {
            return true;
        }
        if(array[m] < v) {
            a = m + 1;
        } else {
            b = m - 1;
        }
    }

    return false;
}

int aavg(int *array, size_t l) {
    int avg = 0;
    for(size_t i = 0; i < l; i++) {
        avg += array[i];
    }

    return avg / l;
}

void print_array(int *array, size_t l) {
    for(size_t i = 0; i < l; i++) {
        std::cout << array[i] << " ";        
    }
    std::cout << std::endl;
}

//void partition(int *array, )
size_t partition(int *array, size_t l) {
    int pivot = array[int(floor((l-1)/2))];
    int a = -1;
    int b = l;
    for(;;) {
        do {
            a++;
        } while(array[a] < pivot);

        do {
            b--;
        } while(array[b] > pivot);

        if(a >= b) return b;

        std::swap(array[a], array[b]);
    }
}

void quicksort(int *array, size_t l) {
    if(l <= 1) return;

    size_t pivot_p = partition(array, l);

    quicksort(array, pivot_p + 1);
    quicksort(array + pivot_p + 1, l - (pivot_p + 1));
}

int threesum(int *array, size_t l) {
    quicksort(array, l);
    int c = 0;
    int m = -1;
    for(size_t i = 1; i < l; i++) {
        if(array[i - 1] < 0 != array[i] < 0) {
            m = i;
            break;
        }
    }
    if(m == -1) return 0; // tutti positivi o tutti negativi
    //coppie negative  e ricerca tra positivi
    for(size_t i = 0; i < m; i++) {
        for(size_t j = 0; j < m - 1; j++) {
            c += i != j && binsearch(array, m, l, -array[i] - array[j]);
        }
    }
    //coppie positive e ricerca tra negativi
    for(size_t i = m; i < l; i++) {
        for(size_t j = m; j < l - 1; j++) {
            c += i != j && binsearch(array, 0, m - 1, -array[i] - array[j]);
        }
    }

    return c;
}

int brutforce_threesum(int *array, size_t l) {
    int c = 0;

    for(size_t i = 0; i < l; i++) {
        for(size_t j = 0; j < l-1; j++) {
            for(size_t k = 0; k < l-2; k++) {
                c += (array[i]+array[j]+array[k]) == 0 && k != j && j != i && i != k;
            }
        }
    }

    return c;
}

int main() {
    int a[] = {1, 3, 3, -6};

    std::cout << threesum(a, 4) << std::endl;
    print_array(a, 4);
    std::cout << "bruteforce " << brutforce_threesum(a, 4) << std::endl;
}
