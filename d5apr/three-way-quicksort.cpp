#include <iostream>
#include <string>
#include <vector>

#include <cmath>

using namespace std;

char c_or_zero(string &s, size_t i) {
    if(s.size() <= i) return 0;
    return s[i];
}

//non completa
size_t partition(string *array, size_t l, size_t sc) {
    char pivot = c_or_zero(array[int(floor((l-1)/2))], sc);
    int a=0, b = -1, c = l, d = l - 1;
    for(;;) {
        do {
            b++;
        } while(c_or_zero(array[b], sc) < pivot);

        do {
            c--;
        } while(c_or_zero(array[c], sc) > pivot);

        if(b >= c) return c;

        std::swap(array[b], array[c]);
    }
}

void twquick_sort(vector<string> &v, size_t sc = 0) {
    if(v.size() <= 1) return;

    vector<string> less, eq, more;
    char pivot = c_or_zero(v[0], sc);
    for(auto &s: v) {
        if(c_or_zero(s, sc) < pivot) {
            less.push_back(s);
        } else if (c_or_zero(s, sc) > pivot) {
            more.push_back(s);
        } else /*if(s == pivot)*/ {
            eq.push_back(s);
        }
    }

    twquick_sort(less, sc);
    twquick_sort(eq, sc + 1);
    twquick_sort(more, sc);

    v.clear();
    v.insert(v.end(), less.begin(), less.end());
    v.insert(v.end(), eq.begin(), eq.end());
    v.insert(v.end(), more.begin(), more.end());
}

int main() {
    vector<string> v{"casa", "cosa", "boh", "a"};

    twquick_sort(v);

    for(auto &s: v) {
        cout << s << endl;
    }
}
